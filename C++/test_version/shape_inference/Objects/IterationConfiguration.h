#pragma once

#include "..\Objects\Pose3D.h"
#include "..\Objects\StepSize3D.h"

#include "..\Others\shape_inferenceDefines.h"

using namespace shape_inference::Objects;

#ifndef shape_inference_MAX_OBJECT_COUNT
#define shape_inference_MAX_OBJECT_COUNT 20
#endif

#ifndef shape_inference_MAX_VIEW_COUNT
#define shape_inference_MAX_VIEW_COUNT 10
#endif


#ifndef shape_inference_MAX_ITER_COUNT
#define shape_inference_MAX_ITER_COUNT 160
#endif

namespace shape_inference
{
	namespace Objects
	{
		class IterationConfiguration
		{
		public:
			int iterCount;

			int width;
			int height;

			int levelSetBandSize;

			int iterObjectCount[shape_inference_MAX_VIEW_COUNT];
			int iterViewCount;

			int iterObjectIds[shape_inference_MAX_VIEW_COUNT][shape_inference_MAX_OBJECT_COUNT];
			int iterViewIds[shape_inference_MAX_VIEW_COUNT];

			IterationTarget iterTarget[shape_inference_MAX_ITER_COUNT];

			bool useCUDARender;
			bool useCUDAEF;

			IterationConfiguration(void) { 
				int i;
				iterViewCount = 0; iterCount = 1;
				for (i=0; i<shape_inference_MAX_VIEW_COUNT; i++) iterObjectCount[i] = 0;
				for (i=0; i<shape_inference_MAX_ITER_COUNT; i++) iterTarget[i] = ITERATIONTARGET_BOTH;
				useCUDARender = false;
				useCUDAEF = false;
			}
			~IterationConfiguration(void) { 
			} 
		};
	}
}