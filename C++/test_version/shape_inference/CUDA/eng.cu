#include "CUDAEngine.h"

#include "CUDAData.h"
#include "CUDADT.h"
#include "CUDAConvolution.h"
#include "CUDAScharr.h"
#include "CUDAEF.h"
#include "CUDARend.h"

CUDAData *cudaData;

void initialiseCUDA(int width, int height, float* HV_func, int HV_funcSize)
{
	cudaData = new CUDAData();

	initialiseRend(width, height);
	initialiseScharr(width, height);
	initialiseDT(width, height);
	initialiseConvolution(width, height);
	initialiseEF(width, height, HV_func, HV_funcSize);

}

void shutdownCUDA()
{
	shutdownRend();
	shutdownScharr();
	shutdownDT();
	shutdownConvolution();
	shutdownEF();
}

void registerObjectImage(Object3D* object, View3D* view, bool renderingFromGPU, bool isMultobject)
{
	int viewId = view->viewId;

	int *roiGenerated = object->roiGenerated[viewId];
	int *roiNormalised = object->roiNormalised[viewId];

	int widthROI, heightROI, widthFull;
	widthROI = roiGenerated[4]; heightROI = roiGenerated[5];
	widthFull = view->imageRenderAll->imageFill->width;

	unsigned char *objects;
	unsigned int *zbuffer, *zbufferInverse; 
	cudaMemcpyKind renderingSource;
	if (renderingFromGPU) 
	{
		objects = cudaData->objects;
		zbuffer = cudaData->zbuffer;
		zbufferInverse = cudaData->zbufferInverse;
		renderingSource = cudaMemcpyDeviceToDevice;
	}
	else
	{
		objects = object->imageRender[viewId]->imageObjects->pixels;
		zbuffer = object->imageRender[viewId]->imageZBuffer->pixels;
		zbufferInverse = object->imageRender[viewId]->imageZBufferInverse->pixels;
		renderingSource = cudaMemcpyHostToDevice;
	}

	unsigned char *objectsGPUROI = object->imageRender[viewId]->imageObjects->pixelsGPU;
	unsigned int *zbufferGPUROI = object->imageRender[viewId]->imageZBuffer->pixelsGPU;
	unsigned int *zbufferInverseGPUROI = object->imageRender[viewId]->imageZBufferInverse->pixelsGPU;

	uchar4 *cameraGPU = (uchar4*) view->imageRegistered->pixelsGPU;
	uchar4 *cameraGPUROI = (uchar4*) object->imageCamera[viewId]->pixelsGPU;

	roiNormalised[0] = 0; roiNormalised[1] = 0;
	roiNormalised[2] = roiGenerated[4]; roiNormalised[3] = roiGenerated[5]; 
	roiNormalised[4] = roiGenerated[4]; roiNormalised[5] = roiGenerated[5];

	shape_inferenceSafeCall(cudaMemcpy2D(objectsGPUROI, widthROI, objects + roiGenerated[0] + roiGenerated[1] * widthFull, 
		widthFull, widthROI, heightROI, renderingSource));
	shape_inferenceSafeCall(cudaMemcpy2D(zbufferGPUROI, widthROI * sizeof(uint1), zbuffer + roiGenerated[0] + roiGenerated[1] * widthFull, 
		widthFull * sizeof(uint1), widthROI * sizeof(uint1), heightROI, renderingSource));
	shape_inferenceSafeCall(cudaMemcpy2D(zbufferInverseGPUROI, widthROI * sizeof(uint1), zbufferInverse + roiGenerated[0] + roiGenerated[1] * widthFull, 
		widthFull * sizeof(uint1), widthROI * sizeof(uint1), heightROI, renderingSource));

	shape_inferenceSafeCall(cudaMemcpy2D(cameraGPUROI, widthROI * sizeof(uchar4), cameraGPU + roiGenerated[0] + roiGenerated[1] * widthFull,
		widthFull * sizeof(uchar4), widthROI * sizeof(uchar4), heightROI, cudaMemcpyDeviceToDevice));

	if (isMultobject)
	{
		unsigned char *objectsAll;
		unsigned char *objectsAllGPUROI = object->imageRender[viewId]->imageObjects->pixelsGPU;

		if (renderingFromGPU) { objectsAll = cudaData->objectsAll; renderingSource = cudaMemcpyDeviceToDevice; }
		else { objectsAll = view->imageRenderAll->imageObjects->pixels; renderingSource = cudaMemcpyHostToDevice; }

		shape_inferenceSafeCall(cudaMemcpy2D(objectsAllGPUROI, widthROI, objectsAll + roiGenerated[0] + roiGenerated[1] * widthFull, 
			widthFull, widthROI, heightROI, renderingSource));
	}
}

void objReg_geomDataView(Object3D* object, View3D* view)
{
	float rotationParameters[7];

	object->pose[view->viewId]->rotation->Get(rotationParameters);
	registerObjectGeometricData(rotationParameters, object->invPMMatrix[view->viewId]);

	registerViewGeometricData(view->renderView->invP, view->renderView->projectionParams.all, view->renderView->view);
}

void silhouette_processing(Object3D* object, View3D* view, int bandSize)
{
	int viewId = view->viewId;

	int *roi = object->roiNormalised[viewId];

	unsigned char *objectsGPUROI = object->imageRender[viewId]->imageObjects->pixelsGPU;
	unsigned char *sihluetteGPUROI = object->imageSihluette[viewId]->pixelsGPU;

	float *dtGPUROI = object->dt[viewId]->pixelsGPU;
	int *dtPosXGPUROI = object->dtPosX[viewId]->pixelsGPU;
	int *dtPosYGPUROI = object->dtPosY[viewId]->pixelsGPU;
	float *dtDXGPUROI = object->dtDX[viewId]->pixelsGPU;
	float *dtDYGPUROI = object->dtDY[viewId]->pixelsGPU;

	computeSihluette(objectsGPUROI, sihluetteGPUROI, roi[4], roi[5], 1.0f);
	processDT(dtGPUROI, dtPosXGPUROI, dtPosYGPUROI, sihluetteGPUROI, objectsGPUROI, roi, bandSize);
	computeDerivativeXY(dtGPUROI, dtDXGPUROI, dtDYGPUROI, roi[4], roi[5]);
}

void PROC_n_ret_first_deriv(Object3D* object, View3D* view, bool isMultiobject)
{
	int viewId = view->viewId;

	float dpose[7];

	int *roiNormalised = object->roiNormalised[viewId];
	int *roiGenerated = object->roiGenerated[viewId];

	float2 *histo = (float2*) object->histoVarBin[viewId]->nGPU;

	uchar4 *cameraGPUROI = (uchar4*) object->imageCamera[viewId]->pixelsGPU;

	unsigned char *objectsGPUROI = object->imageRender[viewId]->imageObjects->pixelsGPU;
	unsigned int *zbufferGPUROI = object->imageRender[viewId]->imageZBuffer->pixelsGPU;
	unsigned int *zbufferInverseGPUROI = object->imageRender[viewId]->imageZBufferInverse->pixelsGPU;

	float *dtGPUROI = object->dt[viewId]->pixelsGPU;
	int *dtPosXGPUROI = object->dtPosX[viewId]->pixelsGPU;
	int *dtPosYGPUROI = object->dtPosY[viewId]->pixelsGPU;
	float *dtDXGPUROI = object->dtDX[viewId]->pixelsGPU;
	float *dtDYGPUROI = object->dtDY[viewId]->pixelsGPU;

	processEFD1(dpose, roiNormalised, roiGenerated, histo, cameraGPUROI, objectsGPUROI, isMultiobject, zbufferGPUROI, zbufferInverseGPUROI, 
		dtGPUROI, dtPosXGPUROI, dtPosYGPUROI, dtDXGPUROI, dtDYGPUROI, object->ID_4_OBJ);

	object->dpose[view->viewId]->SetFrom(dpose, 7);
}

void return_processed_silhouette_data(Object3D* object, View3D* view)
{
	int viewId = view->viewId;

	int *roi = object->roiGenerated[viewId];

	float *dtGPUROI = object->dt[viewId]->pixelsGPU;
	int *dtPosXGPUROI = object->dtPosX[viewId]->pixelsGPU;
	int *dtPosYGPUROI = object->dtPosY[viewId]->pixelsGPU;
	float *dtDXGPUROI = object->dtDX[viewId]->pixelsGPU;
	float *dtDYGPUROI = object->dtDY[viewId]->pixelsGPU;

	unsigned char *sihluetteGPUROI = object->imageSihluette[viewId]->pixelsGPU;

	float *dt = object->dt[viewId]->pixels;
	int *dtPosX = object->dtPosX[viewId]->pixels;
	int *dtPosY = object->dtPosY[viewId]->pixels;
	float *dtDX = object->dtDX[viewId]->pixels;
	float *dtDY = object->dtDY[viewId]->pixels;

	unsigned char* sihluette = object->imageSihluette[viewId]->pixels;

	int widthFull, heightFull;
	widthFull = view->imageRenderAll->imageFill->width; heightFull = view->imageRenderAll->imageFill->height;

	memset(dt, 0, widthFull * heightFull * sizeof(float));
	memset(dtPosX, -1, widthFull * heightFull * sizeof(int));
	memset(dtPosY, -1, widthFull * heightFull * sizeof(int));
	memset(sihluette, 0, widthFull * heightFull * sizeof(unsigned char));
	memset(dtDX, 0, widthFull * heightFull * sizeof(float));
	memset(dtDY, 0, widthFull * heightFull * sizeof(float));

	shape_inferenceSafeCall(cudaMemcpy2D(dt + roi[0] + roi[1] * widthFull, widthFull * sizeof(float), 
		dtGPUROI, roi[4] * sizeof(float), roi[4] * sizeof(float), roi[5], cudaMemcpyDeviceToHost));
	shape_inferenceSafeCall(cudaMemcpy2D(dtPosX + roi[0] + roi[1] * widthFull, widthFull * sizeof(int), 
		dtPosXGPUROI, roi[4] * sizeof(int), roi[4] * sizeof(int), roi[5], cudaMemcpyDeviceToHost));
	shape_inferenceSafeCall(cudaMemcpy2D(dtPosY + roi[0] + roi[1] * widthFull, widthFull * sizeof(int), 
		dtPosYGPUROI, roi[4] * sizeof(int), roi[4] * sizeof(int), roi[5], cudaMemcpyDeviceToHost));
	shape_inferenceSafeCall(cudaMemcpy2D(sihluette + roi[0] + roi[1] * widthFull, widthFull * sizeof(unsigned char), 
		sihluetteGPUROI, roi[4] * sizeof(unsigned char), roi[4] * sizeof(unsigned char), roi[5], cudaMemcpyDeviceToHost));
	shape_inferenceSafeCall(cudaMemcpy2D(dtDX + roi[0] + roi[1] * widthFull, widthFull * sizeof(float), 
		dtDXGPUROI, roi[4] * sizeof(float), roi[4] * sizeof(float), roi[5], cudaMemcpyDeviceToHost));
	shape_inferenceSafeCall(cudaMemcpy2D(dtDY + roi[0] + roi[1] * widthFull, widthFull * sizeof(float), 
		dtDYGPUROI, roi[4] * sizeof(float), roi[4] * sizeof(float), roi[5], cudaMemcpyDeviceToHost));
}

void getProcessedDataRenderingAll(View3D* view)
{
	int widthFull = view->imageRenderAll->imageFill->width;
	int heightFull = view->imageRenderAll->imageFill->height;

	unsigned char *fill = view->imageRenderAll->imageFill->pixels;
	unsigned char *objects = view->imageRenderAll->imageObjects->pixels;
	unsigned int *zbuffer = view->imageRenderAll->imageZBuffer->pixels;
	unsigned int *zbufferInverse = view->imageRenderAll->imageZBufferInverse->pixels;

	shape_inferenceSafeCall(cudaMemcpy(fill, cudaData->fillAll, sizeof(unsigned char) * widthFull * heightFull, cudaMemcpyDeviceToHost));
	shape_inferenceSafeCall(cudaMemcpy(objects, cudaData->objectsAll, sizeof(unsigned char) * widthFull * heightFull, cudaMemcpyDeviceToHost));
	shape_inferenceSafeCall(cudaMemcpy(zbuffer, cudaData->zbufferAll, sizeof(unsigned int) * widthFull * heightFull, cudaMemcpyDeviceToHost));
	shape_inferenceSafeCall(cudaMemcpy(zbufferInverse, cudaData->zbufferInverseAll, sizeof(unsigned int) * widthFull * heightFull, cudaMemcpyDeviceToHost));
}

void getProcessedDataRendering(Object3D* object, View3D* view)
{
	int viewId = view->viewId;

	int widthFull = view->imageRenderAll->imageFill->width;
	int heightFull = view->imageRenderAll->imageFill->height;

	unsigned char *fill = object->imageRender[viewId]->imageFill->pixels;
	unsigned char *objects = object->imageRender[viewId]->imageObjects->pixels;
	unsigned int *zbuffer = object->imageRender[viewId]->imageZBuffer->pixels;
	unsigned int *zbufferInverse = object->imageRender[viewId]->imageZBufferInverse->pixels; 

	shape_inferenceSafeCall(cudaMemcpy(fill, cudaData->fill, sizeof(unsigned char) * widthFull * heightFull, cudaMemcpyDeviceToHost));
	shape_inferenceSafeCall(cudaMemcpy(objects, cudaData->objects, sizeof(unsigned char) * widthFull * heightFull, cudaMemcpyDeviceToHost));
	shape_inferenceSafeCall(cudaMemcpy(zbuffer, cudaData->zbuffer, sizeof(unsigned int) * widthFull * heightFull, cudaMemcpyDeviceToHost));
	shape_inferenceSafeCall(cudaMemcpy(zbufferInverse, cudaData->zbufferInverse, sizeof(unsigned int) * widthFull * heightFull, cudaMemcpyDeviceToHost));
}

void renderObjectCUDA(Object3D *object, View3D *view)
{
	int viewId = view->viewId;
	int width = view->imageRenderAll->imageFill->width;
	int height = view->imageRenderAll->imageFill->height;

	Rend3DObject* renderObject = object->renderObject;

	renderObjectCUDA_one_EF((float4*)renderObject->drawingModel[viewId]->verticesGPU, renderObject->drawingModel[viewId]->faces_num, 
		object->ID_4_OBJ, object->pmMatrix[viewId], view->renderView->view, width, height);

	memcpy(object->roiGenerated[viewId], cudaData->roiGenerated, 6 * sizeof(int));
}

void renderObjectAllCUDA(Object3D **objects, int num_of_objects_in_scene, View3D *view)
{
	int ID_4_OBJ_2, viewId, ID_4_OBJ, width, height;

	Object3D* object;
	Rend3DObject* renderObject; Rend3DView* renderView;

	width = view->imageRenderAll->imageFill->width;
	height = view->imageRenderAll->imageFill->height;

	renderView = view->renderView;
	object = objects[0]; renderObject = object->renderObject;

	viewId = view->viewId; ID_4_OBJ = object->ID_4_OBJ;

	renderObjectCUDA_all_EF((float4*)renderObject->drawingModel[viewId]->verticesGPU, renderObject->drawingModel[viewId]->faces_num,
		ID_4_OBJ, object->pmMatrix[viewId], renderView->view, width, height, true);

	for (ID_4_OBJ_2 = 1; ID_4_OBJ_2<num_of_objects_in_scene; ID_4_OBJ_2++)
	{
		object = objects[ID_4_OBJ_2]; renderObject = object->renderObject; ID_4_OBJ = object->ID_4_OBJ;

		renderObjectCUDA_all_EF((float4*)renderObject->drawingModel[viewId]->verticesGPU, renderObject->drawingModel[viewId]->faces_num,
			ID_4_OBJ, object->pmMatrix[viewId], renderView->view, width, height, false);
	}

	memcpy(view->roiGeneratedAll, cudaData->roiGeneratedAll, 6 * sizeof(int));
}