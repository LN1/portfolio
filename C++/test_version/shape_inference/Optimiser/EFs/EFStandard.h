#pragma once

#include "..\..\Optimiser\EFs\IEnergyFunction.h"

namespace shape_inference
{
	namespace Optimiser
	{
		class EFStandard: public IEnergyFunction
		{
		private:
			void GetFirstDerivativeValues_CPU_6DoF(Object3D ***objects, int *objectCount, View3D** views, int viewCount, IterationConfiguration* iterConfig);

		public:
			void PrepareIteration(Object3D ***objects, int *objectCount, View3D** views, int viewCount, IterationConfiguration* iterConfig);
			void GetFirstDerivativeValues(Object3D ***objects, int *objectCount, View3D** views, int viewCount, IterationConfiguration* iterConfig);

			EFStandard(void);
			~EFStandard(void);
		};
	}
}