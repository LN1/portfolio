#pragma once

#include "..\..\Utils\ImageUtils.h"

#include "..\..\Primitives\Vector2D.h"

#include "..\..\Others\shape_inferenceDefines.h"

using namespace shape_inference::Primitives;
using namespace shape_inference::Utils;

namespace Renderer
{
	namespace Engine
	{
		class LineAlgorithm
		{
			static LineAlgorithm* instance;
		public:
			static LineAlgorithm* Instance(void) {
				if (instance == NULL) instance = new LineAlgorithm();
				return instance;
			}

			int sgn(int num) { if (num > 0) return(1); else if (num < 0) return(-1); else return(0); }

			void DrawLine(ImageUChar *image, int x1, int y1, int x2, int y2, VBYTE color);

			LineAlgorithm(void);
			~LineAlgorithm(void);
		};
	}
}